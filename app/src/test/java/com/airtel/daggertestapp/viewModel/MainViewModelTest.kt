package com.airtel.daggertestapp.viewModel

import com.airtel.daggertestapp.BaseTest
import com.airtel.daggertestapp.model.Country
import com.airtel.daggertestapp.network.CountryService
import io.reactivex.Single
import org.junit.Assert
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito

/**
 * Created by SURYA N on 4/6/20.
 */
class MainViewModelTest : BaseTest(){

    @Mock
    lateinit var service: CountryService

    @InjectMocks
    lateinit var viewModel: MainViewModel

    private var testScheduler : Single<List<Country>> ?= null

    private fun setSchedulerData(){
        testScheduler = Single.just(arrayListOf(Country("India","")))
    }


    @Test
    fun getCountriesList() {
        setSchedulerData()
        Mockito.`when`(service.getCountries()).thenReturn(testScheduler)
        viewModel.getCountryList()
        Assert.assertEquals(1,viewModel.countryLiveData.value?.size)
    }

    @Test
    fun getCountriesFail(){
        testScheduler = Single.error(Throwable())
        Mockito.`when`(service.getCountries()).thenReturn(testScheduler)
        viewModel.getCountryList()
        Assert.assertEquals(null,viewModel.countryLiveData.value?.size)
    }
}