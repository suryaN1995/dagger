package com.airtel.daggertestapp.di

import com.airtel.daggertestapp.network.ApiService
import com.airtel.daggertestapp.network.CountryService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by SURYA N on 4/6/20.
 */
@Module
open class ApiModule {

    private val BASE_URL = "https://raw.githubusercontent.com/"
    private lateinit var apiService : ApiService

    @Provides
    @Singleton
    @Synchronized
    open fun providesApiService() : ApiService {
        return if(this::apiService.isInitialized)
            apiService
        else{
            apiService= createApiInstance()
            apiService
        }
    }

    private fun createApiInstance(): ApiService{
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(interceptor) // same for .addInterceptor(...)
            .connectTimeout(30, TimeUnit.SECONDS) //Backend is really slow
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiService::class.java)
    }

    @Provides
    fun providesCountry() = CountryService()

}