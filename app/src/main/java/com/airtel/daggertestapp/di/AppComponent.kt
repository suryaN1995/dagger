package com.airtel.daggertestapp.di

import com.airtel.daggertestapp.DaggerApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class,ApiModule::class])
interface AppComponent {
    fun inject(app: DaggerApplication)

    @Component.Builder
    interface Builder {
        fun appModule(appModule: AppModule): Builder
        fun apiModule(apiModule: ApiModule): Builder
        fun build(): AppComponent
    }
}