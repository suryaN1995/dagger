package com.airtel.daggertestapp.di

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.airtel.daggertestapp.viewModel.MainViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by SURYA N on 8/6/20.
 */
@Module
class ViewModelModule {

    @Provides
    @Singleton
    fun providesMainViewModel(activity: AppCompatActivity): MainViewModel {
        return ViewModelProviders.of(activity)[MainViewModel::class.java]
    }
}