package com.airtel.daggertestapp.di

import com.airtel.daggertestapp.DaggerApplication
import com.airtel.daggertestapp.network.CountryService
import com.airtel.daggertestapp.viewModel.MainViewModel
import dagger.Component
import dagger.Subcomponent
import javax.inject.Singleton

/**
 * Created by SURYA N on 4/6/20.
 */
@Singleton
@Component(modules = [ApiModule::class])
interface ApiComponent {
    fun inject(service: CountryService)
    fun inject(viewModel: MainViewModel)
}