package com.airtel.daggertestapp.di

import androidx.appcompat.app.AppCompatActivity
import com.airtel.daggertestapp.view.MainActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * Created by SURYA N on 8/6/20.
 */
@Singleton
@Component(modules = [ViewModelModule::class])
interface ViewModelComponent {

    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun activity(activity: AppCompatActivity): Builder
        fun build(): ViewModelComponent
    }
}