package com.airtel.daggertestapp.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.airtel.daggertestapp.R
import com.airtel.daggertestapp.custom.loadImage
import com.airtel.daggertestapp.custom.progressDrawable
import com.airtel.daggertestapp.databinding.AdapterMainBinding
import com.airtel.daggertestapp.model.Country

/**
 * Created by SURYA N on 4/6/20.
 */
class MainAdapter(var countryList: List<Country>) :
    RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    class MainViewHolder(val binding: AdapterMainBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(country: Country) {
            binding.ivImg.loadImage("https:${country.url}", progressDrawable(binding.root.context))
            binding.tvCountry.text = country.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MainViewHolder(
        DataBindingUtil.inflate<AdapterMainBinding>(
            LayoutInflater.from(parent.context),
            R.layout.adapter_main,
            parent,
            false
        )
    )

    override fun getItemCount() = countryList.size

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(countryList[position])
    }

}