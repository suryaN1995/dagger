package com.airtel.daggertestapp.view

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.airtel.daggertestapp.R
import com.airtel.daggertestapp.custom.ViewModelProviderFactory
import com.airtel.daggertestapp.databinding.ActivityMainBinding
import com.airtel.daggertestapp.di.DaggerViewModelComponent
import com.airtel.daggertestapp.viewModel.MainViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import javax.inject.Inject

open class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding ?= null
    @Inject
    lateinit var viewModel: MainViewModel
    private var adapter: MainAdapter ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        DaggerViewModelComponent.builder().activity(this).build().inject(this)
        initView()
    }

    private fun initView() {
        adapter = MainAdapter(arrayListOf())
        Glide.with(this).asGif().load(R.raw.scan_qr).into(object : CustomTarget<GifDrawable>(){
            override fun onLoadCleared(placeholder: Drawable?) {

            }

            override fun onResourceReady(
                resource: GifDrawable,
                transition: Transition<in GifDrawable>?
            ) {
                binding?.gif?.setImageDrawable(resource)
                resource.start()
            }

        })
        viewModel.countryLiveData.observe(this, Observer {countryList ->
            countryList?.let {
                adapter?.countryList = it
                adapter?.notifyDataSetChanged()
            }
        })
        binding?.rvList?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = this@MainActivity.adapter
        }
        viewModel.getCountryList()
    }
}