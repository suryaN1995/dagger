package com.airtel.daggertestapp.network

import com.airtel.daggertestapp.di.DaggerApiComponent
import com.airtel.daggertestapp.model.Country
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by SURYA N on 4/6/20.
 */
class CountryService {

    @Inject
    lateinit var api: ApiService

    init {
        DaggerApiComponent.create().inject(this)
    }

    fun getCountries(): Single<List<Country>> {
        return api.getCountries()
    }
}