package com.airtel.daggertestapp.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.airtel.daggertestapp.custom.loadSub
import com.airtel.daggertestapp.di.DaggerApiComponent
import com.airtel.daggertestapp.model.Country
import com.airtel.daggertestapp.network.CountryService
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by SURYA N on 4/6/20.
 */
class MainViewModel : ViewModel() {

    @Inject
    lateinit var api : CountryService
    private val disposable = CompositeDisposable()

    init {
        DaggerApiComponent
            .builder()
            .build()
            .inject(this)
    }

    val countryLiveData = MutableLiveData<List<Country>>().apply {
        postValue(null)
    }


    fun getCountryList() {
        disposable.add(
        api.getCountries().loadSub().subscribeWith(object :
            DisposableSingleObserver<List<Country>>() {
            override fun onSuccess(data: List<Country>) {
                countryLiveData.value = data
            }

            override fun onError(e: Throwable) {
                countryLiveData.value = null
            }
        }))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}