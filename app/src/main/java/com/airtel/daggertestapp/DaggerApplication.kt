package com.airtel.daggertestapp

import android.app.Activity
import android.app.Application
import com.airtel.daggertestapp.di.ApiModule
import com.airtel.daggertestapp.di.AppModule
import com.airtel.daggertestapp.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by SURYA N on 8/6/20.
 */
open class DaggerApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        initDagger().inject(this)
    }

    open fun initDagger() =
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .apiModule(ApiModule())
            .build()

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

}