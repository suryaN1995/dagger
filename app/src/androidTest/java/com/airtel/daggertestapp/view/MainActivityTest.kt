package com.airtel.daggertestapp.view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.airtel.daggertestapp.R
import com.airtel.daggertestapp.di.ApiModule
import com.airtel.daggertestapp.model.Country
import com.airtel.daggertestapp.network.ApiService
import com.airtel.daggertestapp.network.CountryService
import com.airtel.daggertestapp.utils.BaseTest
import com.airtel.daggertestapp.utils.RecyclerViewMatcher
import com.airtel.daggertestapp.utils.getOrAwaitValue
import com.airtel.daggertestapp.viewModel.MainViewModel
import dagger.Module
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import kotlinx.android.synthetic.main.activity_main.view.*
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Spy

/**
 * Created by SURYA N on 5/6/20.
 */
class MainActivityTest : BaseTest() {

    @Rule
    @JvmField
    var activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

    lateinit var api: ApiService

    @InjectMocks
    lateinit var viewModel: MainViewModel

    override fun setUp() {
        super.setUp()
        api = Mockito.mock(ApiService::class.java)
    }


    private fun setApiValues() {
        val country = Country("India","")
        val countriesList = arrayListOf<Country>()
        for (i in 0 until 20)
            countriesList.add(country)
        Mockito.`when`(api.getCountries()).thenReturn(Single.just(countriesList))
    }

    @Test
    fun launchApp() {
        setApiValues()
        //onData(RecyclerViewMatcher(R.id.rv_list).atPosition(0)).perform()
    }

}