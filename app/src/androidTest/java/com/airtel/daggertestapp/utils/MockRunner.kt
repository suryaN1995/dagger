package com.airtel.daggertestapp.utils

import android.app.Application
import android.content.Context
import android.os.Bundle
import android.os.StrictMode
import android.view.WindowManager.LayoutParams.*
import androidx.test.runner.AndroidJUnitRunner
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import com.airtel.daggertestapp.DaggerApplication


/**
 * Created by SURYA N on 8/6/20.
 */
class MockRunner : AndroidJUnitRunner() {

    override fun onCreate(arguments: Bundle) {
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build())
        super.onCreate(arguments)
    }

    @Throws(InstantiationException::class, IllegalAccessException::class, ClassNotFoundException::class)
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        return super.newApplication(cl, DaggerApplication::class.java.name, context)
    }
}
