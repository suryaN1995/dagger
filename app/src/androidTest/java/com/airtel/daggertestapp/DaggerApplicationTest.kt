package com.airtel.daggertestapp

import android.app.Application
import com.airtel.daggertestapp.di.ApiModule
import com.airtel.daggertestapp.di.AppComponent
import com.airtel.daggertestapp.di.AppModule
import com.airtel.daggertestapp.di.DaggerAppComponent
import com.airtel.daggertestapp.network.ApiService
import dagger.Module
import org.mockito.Mock
import org.mockito.Mockito

/**
 * Created by SURYA N on 8/6/20.
 */
class DaggerApplicationTest : DaggerApplication() {

    override fun initDagger(): AppComponent {
        return DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .apiModule(MockApiModule())
            .build()
    }

    @Module
    private class MockApiModule : ApiModule(){
        override fun providesApiService(): ApiService {
            return Mockito.mock(ApiService::class.java)
        }
    }


}